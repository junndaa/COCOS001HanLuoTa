import { _decorator, Component, Node, SpriteAtlas, Sprite, UITransform, Button, log, NodeEventType, Vec3 } from 'cc';
import { game } from './game';
const { ccclass, property } = _decorator;

@ccclass('block')
export class block extends Component {

    @property(SpriteAtlas)
    colorAtlas: SpriteAtlas

    @property({ type: game, visible: false })
    gameObject_game: game

    initial_postion: Vec3

    onLoad() {
        this.node.on(NodeEventType.TOUCH_START, this.onTouchStart, this)
        this.node.on(NodeEventType.TOUCH_MOVE, this.onTouchMove, this)
        this.node.on(NodeEventType.TOUCH_END, this.onTouchEnd, this)
    }

    onDestory(e) {
        this.node.off(NodeEventType.TOUCH_START, this.onTouchStart, this)
        this.node.off(NodeEventType.TOUCH_MOVE, this.onTouchMove, this)
        this.node.on(NodeEventType.TOUCH_END, this.onTouchEnd, this)
    }

    init(blockIndex) {
        this.node.getComponent(Sprite).spriteFrame = this.colorAtlas.getSpriteFrame(blockIndex)
        this.node.getComponent(UITransform).width = 80 + 40 * blockIndex
    }

    onTouchStart(e) {
        //this.initial_postion = this.node.position 错误，这会导致initial_postion的值一直随this.node.position的变化而变化
        this.initial_postion = new Vec3(this.node.position.x,this.node.position.y,0)

    }
    onTouchMove(e) {
        let delta = e.getUIDelta();
        let begin_postion = this.node.getPosition();
        this.node.setPosition(new Vec3(begin_postion.x + delta.x, begin_postion.y + delta.y, 0))
    }
    //拖动结束，放置滑块
    onTouchEnd(e) {
        let placeFlag = this.gameObject_game.placeBlock(this.node)
        if (!placeFlag) {
            this.node.position = this.initial_postion
        }
    }

}

