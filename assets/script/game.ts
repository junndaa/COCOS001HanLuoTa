import { _decorator, Component, Node, Prefab, instantiate, Vec3, log, UITransform } from 'cc';
import { block } from './block';
const { ccclass, property } = _decorator;

export class BlockNode extends Node {
    //所在木桩索引
    baseIndex: number;
    //所在block的二维数组索引 3，2，1，0 【3是最长的】
    blockIndex: number;
}

@ccclass('game')
export class game extends Component {

    @property(Prefab)
    blockPrefab: Prefab

    @property(Node)
    blockLayerNode: Node

    //3个木桩的⊥⊥⊥
    @property([Node])
    baseNodeArr: [Node]

    //block的二位数组,3*n
    blockNodeArr_2D: BlockNode[][] = []
    //[[0,1,2][][]]
    //[[1,2][0][]]

    onLoad() {
        this.blockPrefab["baseIndex"] = Number
        this.initBlock(4);
    }

    //初始化小方块 到第一个木桩 和 二维数组里面去
    initBlock(num) {
        //初始化放置方块的数组[][][]
        this.blockNodeArr_2D.push([])
        this.blockNodeArr_2D.push([])
        this.blockNodeArr_2D.push([])
        for (let i = 0; i < num; i++) {
            let blockNode: Node = instantiate(this.blockPrefab);
            this.blockLayerNode.addChild(blockNode);
            let BlockNodeNew: BlockNode = blockNode as BlockNode
            let blockIndex = -i + num - 1
            BlockNodeNew.baseIndex = 0;
            BlockNodeNew.blockIndex = blockIndex; // 3,2,1,0
            //给预制体以改脚本组件
            BlockNodeNew.getComponent(block).gameObject_game = this
            BlockNodeNew.setPosition(this.baseNodeArr[0].getPosition().x, -120 + i * 44, 0)
            BlockNodeNew.getComponent(block).init(blockIndex)
            console.log(this.blockNodeArr_2D)
            this.blockNodeArr_2D[0].push(BlockNodeNew)
        }
    }

    /**
     * 判断方块移动的位置是否合法，if(合法) return 当前木桩的索引 index , else return -1;
     * @param pos 
     * @returns number
     */
    baseIndexCheck(pos: Vec3) {
        for (let i = 0; i < this.baseNodeArr.length; i++) {
            let baseNode = this.baseNodeArr[i]
            //如果 方块的位置在 木头之间的话，输出木桩的index索引
            if (pos.x >= baseNode.position.x - baseNode.getComponent(UITransform).width / 2
                && pos.x <= baseNode.position.x + baseNode.getComponent(UITransform).width / 2) {
                log("方块到达了指定的木桩点是" + i)
                return i;
            }
        }
        log("方块位置不合法")
        return -1;
    }

    /**
     * 放置方块
     * @param blockNode 
     * @returns 
     */
    placeBlock(blockNode) {
        let newbaseIndex = this.baseIndexCheck(blockNode.position)
        if (newbaseIndex == -1) {
            return false;
        }
        //判断移动的方块是不是原来木桩上的最顶点的方块
        let oldTopBlockNode = this.blockNodeArr_2D[blockNode.baseIndex][this.blockNodeArr_2D[blockNode.baseIndex].length-1]
        if(oldTopBlockNode.blockIndex != blockNode.blockIndex){
            return false;
        }
        //获取新地方的木桩的方块个数
        let blockNums = this.blockNodeArr_2D[newbaseIndex].length

        //判断blockNode的长度是否小于木桩上最上方的方块的长度
        if (blockNums > 0) {
            let outBlockNode = this.blockNodeArr_2D[newbaseIndex][blockNums - 1]
            if (outBlockNode.blockIndex < blockNode.blockIndex) {
                return false;
            }
        }       
        //放置方块     //计算方块节点放到指定木桩的x轴
        let pos_x = this.baseNodeArr[newbaseIndex].position.x
        let pos_y = 0
        //计算方块到指定节点的y轴     //判断该木桩【⊥】上是否有方块【▇】
        let blockNumber = blockNums
        if (blockNumber == 0) {
            pos_y = -120
        } else {
            pos_y = -120 + blockNumber * 44
        }
        blockNode.setPosition(new Vec3(pos_x, pos_y, 0))
        //更新方块的木桩索引  //更新二维数组里面的方块位置
        let oldBaseIndex = blockNode.baseIndex
        blockNode.baseIndex = newbaseIndex
        this.blockNodeArr_2D[oldBaseIndex].pop()
        this.blockNodeArr_2D[newbaseIndex].push(blockNode)
        return true;
    }
}

